package com.allstate.crm.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomerEntityTest {

    private Customer customer;

    @BeforeEach
    void setUp() {
        customer = new Customer(3, "Gouti", 30, "guma@outlook.com","111, navi block", "123456");

    }

    @Test
    public void getCustomerId() {
        assertEquals(3, customer.getCustomerId());
    }

    @Test
    public void getName() {
        assertEquals("Gouti", customer.getName());
    }

    @Test
    public void getAge() {
        assertEquals(30, customer.getAge());
    }

    @Test
    public void getEmail() {
        assertEquals("guma@outlook.com", customer.getEmail());
    }
    @Test
    public void getAddress() {
        assertEquals("111, navi block", customer.getAddress());
    }
    @Test
    public void getPhoneNumber() {
        assertEquals("123456", customer.getPhoneNumber());
    }
}
