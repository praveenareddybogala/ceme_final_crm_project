package com.allstate.crm.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class InteractionEntityTest {

    private Interaction interaction;

    @BeforeEach
    void setUp() {
        interaction = new Interaction(30, 1, "issues with headset", new Date(),"Call");
    }

    @Test
    public void getCustomerId() {
        assertEquals(1, interaction.getCustomerId());
    }

    @Test
    public void getInteractionId() {
        assertEquals(30, interaction.getInteractionId());
    }

    @Test
    public void getInteractionDetails(){
        assertEquals("issues with headset", interaction.getInteractionDetails());
    }

    @Test
    public void getInteractionType(){
        assertEquals("Call", interaction.getInteractionType());
    }

    @Test
    public void getInteractionDateAndTime(){
        assertNotNull( interaction.getInteractionDateAndTime());
    }
}
