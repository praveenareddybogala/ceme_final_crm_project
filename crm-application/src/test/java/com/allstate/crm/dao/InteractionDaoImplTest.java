package com.allstate.crm.dao;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ComponentScan("com.allstate.crm")
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class InteractionDaoImplTest {
    @Autowired
    InteractionDao interactionDao;

    @Autowired
    MongoTemplate tpl;

    private Interaction interaction;

    @Test
    public void testAddInteraction() {
        interaction = new Interaction(30, 2, "issues with headset", new Date(),"Call");
        assertEquals(1L,interactionDao.addInteraction(interaction));
    }

    @Test
    public void testFindInteractionByCustomerId(){
        assertNotNull(interactionDao.findInteraction(2));
    }

    @Test
    public void testDeleteInteractionByCustomerId(){
        assertEquals(1L, interactionDao.deleteInteration(2));
    }

    @Test
    public void testUpdateInteraction() {
        interaction = new Interaction(30, 2, "payment issues", new Date(),"text");
        assertEquals(1L, interactionDao.updateInteraction(interaction));
    }
}
