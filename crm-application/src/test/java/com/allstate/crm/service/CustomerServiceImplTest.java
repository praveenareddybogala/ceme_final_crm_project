package com.allstate.crm.service;


import com.allstate.crm.entities.Customer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ComponentScan("com.allstate.crm")
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CustomerServiceImplTest {

    @Autowired
    private CustomerServiceImpl customerService;

    private Customer customer;

    @Test
    public void testAddCustomer() {
        customer = new Customer(1, "JOHN", 51, "avb@yahoo.com", "123,East plaza", "915416482");
        assertEquals(1L,  customerService.addCustomer(customer));
    }

    @Test
    public void testFindCustomerById() {
        assertEquals(2, customerService.findCustomerById(2).getCustomerId());
    }

    @Test
    public void testFindCustomerByName(){
        List<Customer> customers=customerService.findCustomerByName("Marry");
        assertNotNull(customers);
    }

    @Test
    public void testFindAllCustomer(){
        List<Customer> customers=customerService.findAllCustomer();
        assertNotNull(customers);
    }

    @Test
    public void testDeleteCustomer(){
        assertEquals(1L, customerService.deleteCustomer(6));
    }

    @Test
    public void testUpdateCustomer() {
        customer = new Customer(1, "JOHN", 29, "avb@gmail.com", "123,South plaza", "915416482");
        assertEquals(1L, customerService.updateCustomer(customer));
    }

}
