package com.allstate.crm.dao;

import com.allstate.crm.entities.Interaction;
import com.allstate.crm.exceptions.InteractionException;

import java.util.List;

public interface InteractionDao {

    List<Interaction> findInteraction(int customerId);
    int addInteraction(Interaction interaction) throws InteractionException;
    int updateInteraction(Interaction interaction) throws InteractionException;
    int deleteInteration(int customerId);
}
