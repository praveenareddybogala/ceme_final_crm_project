package com.allstate.crm.service;

import com.allstate.crm.entities.Interaction;
import com.allstate.crm.exceptions.InteractionException;

import java.util.List;

public interface InteractionService {
    List<Interaction> findInteraction(int customerId);
    int addInteraction(Interaction interaction) throws InteractionException;
    int deleteInteraction(int customerId) throws InteractionException;
    int updateInteraction(Interaction interaction) throws InteractionException;
}
