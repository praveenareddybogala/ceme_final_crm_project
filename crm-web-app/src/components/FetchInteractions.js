import { useDispatch } from "react-redux";
import { fetchInteractions } from "../actions";
import { useHistory } from "react-router-dom";

const FetchInteractions = ({
	customerId,
	name,
	age,
	email,
	address,
	phoneNumber,
}) => {
	const history = useHistory();
	const dispatch = useDispatch();
	const handleOnClick = () => {
		//e.preventDefault();
		// dispatch(fetchInteractions(customerDetails.customerId))
		// 	.then(() => {
		// 		//document.getElementById("msg").innerHTML = "Successfully Added";
		// 	})
		// 	.finally(() => {
		history.push(
			"/interactions" +
				"/" +
				customerId +
				"/" +
				name +
				"/" +
				email +
				"/" +
				phoneNumber
		);
		// });
		//toggle(!visible);
	};
	return (
		// !visible &&
		<button
			type="button"
			className="btn btn-sm btn-outline-secondary"
			onClick={() => handleOnClick()}
		>
			View
		</button>
	);
};
export default FetchInteractions;
