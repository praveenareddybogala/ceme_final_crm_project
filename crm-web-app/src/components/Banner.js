import React from "react";
import CustomerApiStatus from "./CustomerApiStatus";

const Banner = () => (
  <section className="jumbotron text-center">
    <div className="container">
      <h1 className="jumbotron-heading">Customer Relationship Management</h1>
      <p className="lead text-muted">CRM.</p>
    </div>
    <div class="jumbotron">
      <h1 class="display-4">Hello!</h1>
      <p class="lead">
        You can add customer issues and maintain interactions and resolution
        provided to issues.
      </p>

      <p>Customer Satisfaction is our Mission!!!.</p>
      <div class="btn btn-primary btn-lg" role="button">
        <CustomerApiStatus />
      </div>
    </div>

    <div className="footer">
      <span className="italic">© 2020 Copyright:</span>
      <a href=""> Team 1-CEME</a>
    </div>
  </section>
);

export default Banner;
