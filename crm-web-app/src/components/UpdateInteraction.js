import Modal from "react-bootstrap/Modal";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateInteractions } from "../actions";
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";

const UpdateInteraction = ({
	status,
	setStatus,
	customerId,
	interactionId,
	interactionDetails,
	interactionDateAndTime,
	interactionType,
	name,
	email,
	phoneNumber,
}) => {
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	const dispatch = useDispatch();
	const history = useHistory();
	const interactions = useSelector((state) => state.interactions.entities);
	const selectedInteraction = interactions.data.filter(
		(i) => i.interactionId == interactionId
	)[0];

	const [interaction, setInteraction] = useState(selectedInteraction);

	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch(
			updateInteractions({
				customerId: interaction.customerId,
				interactionId: interaction.interactionId,
				interactionDetails: interaction.interactionDetails,
				interactionType: interaction.interactionType,
				interactionDateAndTime: interaction.interactionDateAndTime,
			})
		);
		setStatus(!status);
		history.push(
			"/interactions" +
				"/" +
				customerId +
				"/" +
				name +
				"/" +
				email +
				"/" +
				phoneNumber
		);
		handleClose();
	};

	return (
		<>
			<button
				variant="primary"
				onClick={handleShow}
				className="btn btn-sm btn-outline-secondary"
			>
				Update Interaction
			</button>

			<Modal show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title>Fill Details </Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div>
							<label htmlFor="exampleFormControlTextarea1">
								Interaction Details:
							</label>
							<textarea
								className="form-control"
								id="exampleFormControlTextarea1"
								rows="3"
								value={interaction.interactionDetails}
								onChange={(e) =>
									setInteraction({
										...interaction,
										interactionDetails: e.target.value,
									})
								}
							></textarea>
						</div>
				</Modal.Body>
				<Modal.Footer>
					<button variant="secondary" onClick={handleClose}>
						Close
					</button>
					<button variant="primary" onClick={handleSubmit}>
						Save Changes
					</button>
				</Modal.Footer>
			</Modal>
		</>
	);
};
export default UpdateInteraction;
