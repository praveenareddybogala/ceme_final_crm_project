import React from "react";
import { useSelector } from "react-redux";

const CustomerApiStatus = () => {
  const apiStatus = useSelector((state) => state.apiStatus.entities);
  const error = useSelector((state) => state.apiStatus.error);
  const loading = useSelector((state) => state.apiStatus.loading);

  if (error) {
    return <div className="d-flex justify-content-center">{error.message}</div>;
  }

  if (loading) {
    return (
      <div className="d-flex justify-content-center">
        <div
          className="spinner-border m-5"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-8">
          <div className="card mb-4 box-shadow" style={{ width: "40rem" }}>
            <h1>
              <span className="badge badge-secondary">{apiStatus}</span>
            </h1>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CustomerApiStatus;
