import React from "react";
const CustomerTableHeader = () => (
	<thead>
		<tr>
			<th scope="col">Customer ID</th>
			<th scope="col">Name</th>
			<th scope="col">Age</th>
			<th scope="col">Email</th>
			<th scope="col">Address</th>
			<th scope="col">Phone Number</th>
			<th scope="col">Actions</th>
		</tr>
	</thead>
);

export default CustomerTableHeader;
