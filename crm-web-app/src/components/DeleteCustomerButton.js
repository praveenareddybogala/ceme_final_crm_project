import React from "react";
import { useDispatch } from "react-redux";
import { deleteCustomer, fetchCustomers } from "../actions";
import { useHistory } from "react-router-dom";

const DeleteCustomerButton = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();

  function handleSubmit() {
    let deleteConfirmed = window.confirm(
      "Are you sure-Deleting the customer- Customer ID:" +
        " " +
        props.customerId
    );
    if (deleteConfirmed) {
      // dispatch deleteCustomer action
      dispatch(deleteCustomer(props.customerId))
        .then(() => {
          console.log("deleteCustomer is successful");
        })
        .catch(() => {})
        .finally(() => {
          console.log("deleteCustomer thunk function is completed");
          dispatch(fetchCustomers());
          history.push("/find");
        });
    }
  }

  return (
    <button
      className="btn btn-sm btn-outline-secondary"
      onClick={() => handleSubmit()}
    >
      Delete
    </button>
  );
};

export default DeleteCustomerButton;
