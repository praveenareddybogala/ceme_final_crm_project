import React, {useState} from "react";

import CreateCustomerForm from "./CreateCustomerForm";
import { useHistory } from "react-router-dom";

const AddCustomerButton = () => {

	const [show, setShow] = useState(false);
	const handleShow = () => setShow(true);
	const history = useHistory();
	// function NavigateToAdd() {
	// 	history.push("/add");
	// }
	return (
		<div className="form-group col-md-5">
			<button className="btn btn-secondary" onClick={() => handleShow()}>
				Add Customer
			</button>
			<br></br>
		</div>
	);
};

export default AddCustomerButton;
