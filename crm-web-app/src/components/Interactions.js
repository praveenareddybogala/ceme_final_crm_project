import "../styling/App.css";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import AddInteraction from "./AddInteraction";
import { fetchInteractions } from "../actions";
import React, { useState, useEffect } from "react";
import UpdateInteraction from "./UpdateInteraction";
import moment from "moment";
const Interactions = ({}) => {
  const customerId = window.location.pathname.split("/")[2];
  const name = window.location.pathname.split("/")[3];
  const email = window.location.pathname.split("/")[4];
  const phoneNumber = window.location.pathname.split("/")[5];

  const [status, setStatus] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchInteractions(customerId)); // dispatch fetchCustomers action
  }, [status]); // run this at start and whenever fetchCustomer is changed

  const interactions = useSelector((state) => state.interactions.entities);
  console.log("interactions", interactions);
  const history = useHistory();

  // const handleNewAdd = (value) => {
  // 	return (
  // 		<AddInteraction
  // 			status={status}
  // 			setStatus={setStatus}
  // 			customerId={value}
  // 		/>
  // 	);
  // 	//history.push("/addInteractions" + "/" + value);

  // 	//setFetchInteraction(!fetchInteraction);
  // };
  // const handleUpdate = (value) => {
  // 	history.push("/updateInteractions" + "/" + value);
  // 	//setFetchInteraction(!fetchInteraction);
  // };
  const displayInteractions = (data) => {
    return data ? (
      <div style={{ maxWidth: "720px" }}>
        <div
          className="my-3 p-3 bg-white rounded box-shadow"
          // style={{ backgroundColor: "purple" }}
        >
          <div
            className="media text-muted pt-3"
            style={{ backgroundColor: "#e9ecef" }}
          >
            <div
              className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray"
              style={{ color: "black", fontSize: "100%" }}
            >
              <div className="d-flex justify-content-between align-items-center w-100">
                <strong className="text-gray-dark">{name}</strong>
                <div>{email}</div>
              </div>
              <span className="d-block">{phoneNumber}</span>
            </div>
          </div>
          <AddInteraction
            status={status}
            setStatus={setStatus}
            customerId={customerId}
            name={name}
            email={email}
            phoneNumber={phoneNumber}
          />
          {/* </div>

				<div className="my-3 p-3 bg-white rounded box-shadow"> */}
          <div
            class="media text-muted pt-3"
            // style={{ backgroundColor: "#FAF0E6" }}
          >
            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
              {data.map((interaction) => {
                console.log("data is", interaction);
                const interactionDateTime = moment
                  .utc(interaction.interactionDateAndTime)
                  .local()
                  .startOf("seconds")
                  .fromNow();
                return (
                  <div>
                    <div className="d-flex justify-content-between align-items-center w-100">
                      <strong className="text-gray-dark">
                        {interactionDateTime}
                      </strong>
                      <div>{interaction.interactionType}</div>
                    </div>
                    <span className="d-block">
                      {interaction.interactionDetails}
                    </span>
                    <UpdateInteraction
                      customerId={customerId}
                      interactionId={interaction.interactionId}
                      status={status}
                      setStatus={setStatus}
                      interactionDetails={interaction.interactionDetails}
                      interactionType={interaction.interactionType}
                      interactionDateAndTime={
                        interaction.interactionDateAndTime
                      }
                      name={name}
                      email={email}
                      phoneNumber={phoneNumber}
                    />
                    <hr />
                    {/* <strong class="d-block text-gray-dark">
											{interaction.interactionDateAndTime}
										</strong>
										{interaction.interactionDetails} */}
                  </div>
                );
              })}
            </p>
          </div>
        </div>
      </div>
    ) : (
      // <div className="col-md-4">
      // 	<div className="card mb-4 box-shadow" style={{ width: "75rem" }}>
      // 		<div className="card-body">
      // 			{/* <div className="close" onClick={() => handleclose()}></div> */}

      // 			{/* <h3>List of {name} interactions</h3> */}
      // 			{/* <Style name={name} email={email} phoneNumber={phoneNumber} /> */}
      // 			<AddInteraction
      // 				status={status}
      // 				setStatus={setStatus}
      // 				customerId={customerId}
      // 			/>
      // 			<table className="table">
      // 				<thead>
      // 					<tr>
      // 						<th scope="col">#</th>
      // 						<th scope="col">Date & Time</th>
      // 						<th scope="col">Interaction Details</th>
      // 						<th scope="col">Interaction Type</th>
      // 					</tr>
      // 				</thead>
      // 				{data.map((interaction, i) => {
      // 					return (
      // 						<tbody>
      // 							<InteractionsMapping
      // 								key={interaction.customerId}
      // 								index={i + 1}
      // 								customerId={interaction.customerId}
      // 								interactionId={interaction.interactionId}
      // 								interactionDetails={interaction.interactionDetails}
      // 								interactionType={interaction.interactionType}
      // 								interactionDateAndTime={
      // 									interaction.interactionDateAndTime
      // 								}
      // 								status={status}
      // 								setStatus={setStatus}
      // 								name={name}
      // 								email={email}
      // 								phoneNumber={phoneNumber}
      // 								// fetchInteraction={fetchInteraction}
      // 								// setFetchInteraction={setFetchInteraction}
      // 							/>
      // 							<tr>
      // 								{/* <div style={{ maxWidth: "720px" }}>
      // 									<div className="my-3 p-3 bg-white rounded box-shadow">
      // 										<div className="media text-muted pt-3">
      // 											<div className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
      // 												<div className="d-flex justify-content-between align-items-center w-100">
      // 													<strong className="text-gray-dark">
      // 														Full Name
      // 													</strong>
      // 													<a href="#">Follow</a>
      // 												</div>
      // 												<span className="d-block">@username</span>
      // 											</div>
      // 										</div>
      // 									</div>
      // 								</div> */}
      // 							</tr>
      // 							{/* <tr scope="row">
      // 								<button
      // 									type="button"
      // 									//className="btn btn-primary"
      // 									onClick={() => handleUpdate(interaction.interactionId)}
      // 								>
      // 									Update Interactions
      // 								</button>
      // 							</tr> */}
      // 						</tbody>
      // 					);
      // 				})}
      // 			</table>
      // 		</div>
      // 	</div>
      // </div>
      <div>
        <div style={{ maxWidth: "720px" }}>
          <div
            className="my-3 p-3 bg-white rounded box-shadow"
            // style={{ backgroundColor: "purple" }}
          >
            <div
              className="media text-muted pt-3"
              style={{ backgroundColor: "#e9ecef" }}
            >
              <div
                className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray"
                style={{ color: "black", fontSize: "100%" }}
              >
                <div className="d-flex justify-content-between align-items-center w-100">
                  <strong className="text-gray-dark">{name}</strong>
                  <div>{email}</div>
                </div>
                <span className="d-block">{phoneNumber}</span>
              </div>
            </div>
          </div>
        </div>
        {/* <Style name={name} email={email} phoneNumber={phoneNumber} /> */}
        No existing Interactions
        <AddInteraction
          status={status}
          setStatus={setStatus}
          customerId={customerId}
          name={name}
          email={email}
          phoneNumber={phoneNumber}
        />
      </div>
    );
  };
  return (
    <div>
      {/* <Style name={name} email={email} phoneNumber={phoneNumber} /> */}
      {displayInteractions(interactions.data)}
    </div>
  );
};
export default Interactions;
