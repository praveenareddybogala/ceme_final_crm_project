import React, { useState } from "react";
import CustomerDetail from "./CustomerDetail.js";
const CustomerData = ({ searchResults, loadList }) => {
	const [visible, setVisibity] = useState(true);
	return (
		<tbody>
			{searchResults.map((customer) => (
				<CustomerDetail
					key={customer.customerId}
					customerId={customer.customerId}
					name={customer.name}
					age={customer.age}
					email={customer.email}
					address={customer.address}
					phoneNumber={customer.phoneNumber}
					visible={visible}
				/>
			))}
		</tbody>
	);
};
export default CustomerData;
