import React from "react";
import { NavLink } from "react-router-dom";
import "../styling/App.css";
const Navbar = () => (
	<ul className="nav">
		<li className="nav-item">
			<NavLink
				exact
				className="nav-link text-secondary"
				to="/"
				activeClassName="app"
			>
				Home
			</NavLink>
		</li>

		<li className="nav-item">
			<NavLink
				exact
				className="nav-link text-secondary"
				to="/find"
				activeClassName="app"
			>
				Customers
			</NavLink>
		</li>
	</ul>
);

export default Navbar;
