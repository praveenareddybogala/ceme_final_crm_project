import React from "react";
import UpdateCustomerForm from "./UpdateCustomerForm";
import { useHistory } from "react-router-dom";

const UpdateCustomerButton = (props) => {
	const history = useHistory();
	function NavigateToUpdate(props) {
		history.push("/update" + "/" + props.customerId);
	}
	return (
		<button
			className="btn btn-sm btn-outline-secondary"
			onClick={() => NavigateToUpdate(props)}
		>
			Edit
		</button>
	);
};

export default UpdateCustomerButton;
