import React from "react";
import UpdateCustomerButton from "./UpdateCustomerButton";
import DeleteCustomerButton from "./DeleteCustomerButton";
import FetchInteractions from "./FetchInteractions";
const CustomerDetail = ({
	customerId,
	name,
	age,
	email,
	address,
	phoneNumber,
	visible,
}) => {
	return (
		visible && (
			<tr>
				<th scope="row" key={customerId}>
					{customerId}
				</th>
				<td key={name}>{name}</td>
				<td key={age}>{age}</td>
				<td key={email}>{email}</td>
				<td key={address}>{address}</td>
				<td key={phoneNumber}>{phoneNumber}</td>
				<td>
					<UpdateCustomerButton
						customerId={customerId}
						name={name}
						age={age}
						email={email}
						address={address}
						phoneNumber={phoneNumber}
					/>
					<DeleteCustomerButton customerId={customerId} />

					<FetchInteractions
						customerId={customerId}
						name={name}
						age={age}
						email={email}
						address={address}
						phoneNumber={phoneNumber}
					/>
				</td>
			</tr>
		)
	);
};

export default CustomerDetail;
