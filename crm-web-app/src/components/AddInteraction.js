import Modal from "react-bootstrap/Modal";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addInteractions } from "../actions";
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";

const AddInteraction = ({
	status,
	setStatus,
	customerId,
	name,
	email,
	phoneNumber,
}) => {
	const dispatch = useDispatch();
	const history = useHistory();
	//const customerId = 1;
	const [interactionId, setInteractionId] = useState("");
	const [interactionDetails, setInteractionDetails] = useState("");
	const [interactionType, setInteractionType] = useState("");
	const [interactionDateAndTime, setInteractionDateAndTime] = useState("");
	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch(
			addInteractions({
				interactionId: interactionId,
				customerId: customerId,
				interactionDetails: interactionDetails,
				interactionDateAndTime: interactionDateAndTime,
				interactionType: interactionType,
			})
		);
		setStatus(!status);
		history.push(
			"/interactions" +
				"/" +
				customerId +
				"/" +
				name +
				"/" +
				email +
				"/" +
				phoneNumber
		);
		handleClose();
		// .then(() => {
		// 	//setStatus(!status);
		// })
		// .finally(() => {
		// 	history.push("/interactions");
		// });
	};

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	return (
		<>
			<button
				variant="primary"
				onClick={handleShow}
				className="btn btn-secondary"
			>
				Add Interaction
			</button>

			<Modal show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title>Fill Details </Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div>
							<label htmlFor="exampleFormControlTextarea1">
								Interaction Details:
							</label>
							<textarea
								className="form-control"
								id="exampleFormControlTextarea1"
								rows="3"
								value={interactionDetails}
								onChange={(e) => setInteractionDetails(e.target.value)}
							></textarea>
						</div>
				
					<div>
						
							<label htmlFor="exampleFormControlSelect1">
								Interaction Type:
							</label>
							<select
								className="form-control"
								id="exampleFormControlSelect1"
								onChange={(e) => setInteractionType(e.target.value)}
							>
								<option>Select One</option>
								<option>Email</option>
								<option>Chat</option>
								<option>Call</option>
							</select>
						
					</div>
				</Modal.Body>
				<Modal.Footer>
					<button variant="secondary" onClick={handleClose}>
						Close
					</button>
					<button variant="primary" onClick={handleSubmit}>
						Save Changes
					</button>
				</Modal.Footer>
			</Modal>
		</>
	);
};
export default AddInteraction;
