const initialState = { entities: [] };

export default (state = initialState, action) => {
	switch (action.type) {
		case "FIND_INTERACTIONS_SUCCESS":
			return { ...state, entities: action.payload, loading: false };
		case "FIND_INTERACTIONS_FAILURE":
			return {
				...state,
				entities: [],
				loading: false,
				error: action.payload,
			};
		case "UPDATE_INTERACTIONS_SUCCESS":
			return { ...state, loading: false };
		case "UPDATE_INTERACTIONS_FAILURE":
			return {
				...state,
				entities: [],
				loading: false,
				error: action.payload,
			};
		case "ADD_INTERACTIONS_SUCCESS":
			return { ...state, loading: false };
		case "ADD_INTERACTIONS_FAILURE":
			return {
				...state,
				entities: [],
				loading: false,
				error: action.payload,
			};
		default:
			return state;
	}
};
