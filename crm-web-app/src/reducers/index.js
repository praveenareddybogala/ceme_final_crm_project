import { combineReducers } from "redux";
import customersReducer from "./customersReducer";
import errorsReducer from "./errorsReducer";
import apiStatusReducer from "./apiStatusReducer";
import updateCustomerReducer from "./updateCustomerReducer";
import interactionsReducer from "./interactionsReducer";
import deleteCustomerReducer from "./deleteCustomerReducer";
const rootReducer = combineReducers({
	customers: customersReducer,
	apiStatus: apiStatusReducer,
	updateCustomer: updateCustomerReducer,
	errors: errorsReducer,
	interactions: interactionsReducer,
	deleteCustomer: deleteCustomerReducer,
});

export default rootReducer;
