const initialState = { entities: [] };

export default (state = initialState, action) => {
	//	console.log(`received ${action.type} dispatch in customersReducer`);
	switch (action.type) {
		case "FETCH_CUSTOMERS_BEGIN":
		case "ADD_CUSTOMER_BEGIN":
			return { ...state, loading: true, error: null };
		case "FETCH_CUSTOMERS_SUCCESS":
			return { ...state, entities: action.payload, loading: false };
		case "FETCH_CUSTOMERS_FAILURE":
			return { ...state, entities: [], loading: false, error: action.payload };
		case "ADD_CUSTOMER_SUCCESS":
			return { ...state, loading: false };
		case "ADD_CUSTOMER_FAILURE":
			return { ...state, loading: false, error: action.payload };
		case "FIND_BY_ID_SUCCESS":
			return { ...state, entities: action.payload };
		case "FIND_BY_ID_FAILURE":
			return { ...state, error: action.payload };
		case "FIND_BY_NAME_SUCCESS":
			return { ...state, entities: action.payload };
		case "FIND_BY_NAME_FAILURE":
			return { ...state, error: action.payload };
		default:
			return state;
	}
};
